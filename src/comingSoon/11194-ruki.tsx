export const data = {
  "v": "5.5.8",
  "fr": 29.9700012207031,
  "ip": 0,
  "op": 61.0000024845809,
  "w": 144,
  "h": 150,
  "nm": "ruki",
  "ddd": 0,
  "assets": [
    {
      "id": "comp_0",
      "layers": [
        {
          "ddd": 0,
          "ind": 1,
          "ty": 4,
          "nm": "Icon Set 6",
          "sr": 1,
          "ks": {
            "o": {
              "a": 0,
              "k": 100,
              "ix": 11
            },
            "r": {
              "a": 0,
              "k": 0,
              "ix": 10
            },
            "p": {
              "a": 0,
              "k": [
                22.04,
                41.388,
                0
              ],
              "ix": 2
            },
            "a": {
              "a": 0,
              "k": [
                0,
                0,
                0
              ],
              "ix": 1
            },
            "s": {
              "a": 0,
              "k": [
                100,
                100,
                100
              ],
              "ix": 6
            }
          },
          "ao": 0,
          "shapes": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0.519,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          -0.518
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          -3.997,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          -3.109,
                          -2.512
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          -0.518,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          3.109,
                          -2.512
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          3.997,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          -0.519
                        ]
                      ],
                      "v": [
                        [
                          13.369,
                          -5.194
                        ],
                        [
                          -13.37,
                          -5.194
                        ],
                        [
                          -14.308,
                          -4.256
                        ],
                        [
                          -14.308,
                          5.194
                        ],
                        [
                          -14.306,
                          5.192
                        ],
                        [
                          -3.323,
                          1.311
                        ],
                        [
                          3.323,
                          1.311
                        ],
                        [
                          14.306,
                          5.192
                        ],
                        [
                          14.308,
                          5.194
                        ],
                        [
                          14.308,
                          -4.255
                        ]
                      ],
                      "c": true
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "fl",
                  "c": {
                    "a": 0,
                    "k": [
                      0.980392158031,
                      0.65098041296,
                      0.101960785687,
                      1
                    ],
                    "ix": 4
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 5
                  },
                  "r": 1,
                  "bm": 0,
                  "nm": "Fill 1",
                  "mn": "ADBE Vector Graphic - Fill",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            }
          ],
          "ip": 0,
          "op": 900.000036657751,
          "st": 0,
          "bm": 0
        },
        {
          "ddd": 0,
          "ind": 2,
          "ty": 4,
          "nm": "Icon Set 5",
          "sr": 1,
          "ks": {
            "o": {
              "a": 0,
              "k": 100,
              "ix": 11
            },
            "r": {
              "a": 0,
              "k": 0,
              "ix": 10
            },
            "p": {
              "a": 0,
              "k": [
                22.039,
                47.139,
                0
              ],
              "ix": 2
            },
            "a": {
              "a": 0,
              "k": [
                0,
                0,
                0
              ],
              "ix": 1
            },
            "s": {
              "a": 0,
              "k": [
                100,
                100,
                100
              ],
              "ix": 6
            }
          },
          "ao": 0,
          "shapes": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          -3.832,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          -3.832
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          -3.832
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          3.832,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "v": [
                        [
                          -20.308,
                          16.946
                        ],
                        [
                          -20.308,
                          -10.007
                        ],
                        [
                          -13.369,
                          -16.946
                        ],
                        [
                          13.369,
                          -16.946
                        ],
                        [
                          20.308,
                          -10.007
                        ],
                        [
                          20.308,
                          14.341
                        ]
                      ],
                      "c": false
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            }
          ],
          "ip": 0,
          "op": 900.000036657751,
          "st": 0,
          "bm": 0
        },
        {
          "ddd": 0,
          "ind": 3,
          "ty": 4,
          "nm": "Icon Set 4",
          "sr": 1,
          "ks": {
            "o": {
              "a": 0,
              "k": 100,
              "ix": 11
            },
            "r": {
              "a": 0,
              "k": 0,
              "ix": 10
            },
            "p": {
              "a": 0,
              "k": [
                22.039,
                12.906,
                0
              ],
              "ix": 2
            },
            "a": {
              "a": 0,
              "k": [
                0,
                0,
                0
              ],
              "ix": 1
            },
            "s": {
              "a": 0,
              "k": [
                100,
                100,
                100
              ],
              "ix": 6
            }
          },
          "ao": 0,
          "ef": [
            {
              "ty": 22,
              "nm": "Stroke",
              "np": 13,
              "mn": "ADBE Stroke",
              "ix": 1,
              "en": 1,
              "ef": [
                {
                  "ty": 10,
                  "nm": "Path",
                  "mn": "ADBE Stroke-0001",
                  "ix": 1,
                  "v": {
                    "a": 0,
                    "k": 0,
                    "ix": 1
                  }
                },
                {
                  "ty": 7,
                  "nm": "All Masks",
                  "mn": "ADBE Stroke-0010",
                  "ix": 2,
                  "v": {
                    "a": 0,
                    "k": 0,
                    "ix": 2
                  }
                },
                {
                  "ty": 7,
                  "nm": "Stroke Sequentially",
                  "mn": "ADBE Stroke-0011",
                  "ix": 3,
                  "v": {
                    "a": 0,
                    "k": 1,
                    "ix": 3
                  }
                },
                {
                  "ty": 2,
                  "nm": "Color",
                  "mn": "ADBE Stroke-0002",
                  "ix": 4,
                  "v": {
                    "a": 0,
                    "k": [
                      1,
                      1,
                      1,
                      1
                    ],
                    "ix": 4
                  }
                },
                {
                  "ty": 0,
                  "nm": "Brush Size",
                  "mn": "ADBE Stroke-0003",
                  "ix": 5,
                  "v": {
                    "a": 0,
                    "k": 2.2,
                    "ix": 5
                  }
                },
                {
                  "ty": 0,
                  "nm": "Brush Hardness",
                  "mn": "ADBE Stroke-0004",
                  "ix": 6,
                  "v": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  }
                },
                {
                  "ty": 0,
                  "nm": "Opacity",
                  "mn": "ADBE Stroke-0005",
                  "ix": 7,
                  "v": {
                    "a": 0,
                    "k": 1,
                    "ix": 7
                  }
                },
                {
                  "ty": 0,
                  "nm": "Start",
                  "mn": "ADBE Stroke-0008",
                  "ix": 8,
                  "v": {
                    "a": 0,
                    "k": 0,
                    "ix": 8
                  }
                },
                {
                  "ty": 0,
                  "nm": "End",
                  "mn": "ADBE Stroke-0009",
                  "ix": 9,
                  "v": {
                    "a": 0,
                    "k": 100,
                    "ix": 9
                  }
                },
                {
                  "ty": 7,
                  "nm": "Spacing",
                  "mn": "ADBE Stroke-0006",
                  "ix": 10,
                  "v": {
                    "a": 0,
                    "k": 15,
                    "ix": 10
                  }
                },
                {
                  "ty": 7,
                  "nm": "Paint Style",
                  "mn": "ADBE Stroke-0007",
                  "ix": 11,
                  "v": {
                    "a": 0,
                    "k": 1,
                    "ix": 11
                  }
                }
              ]
            }
          ],
          "shapes": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          -5.892
                        ],
                        [
                          5.892,
                          0
                        ],
                        [
                          0,
                          5.892
                        ],
                        [
                          -5.892,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          5.892
                        ],
                        [
                          -5.892,
                          0
                        ],
                        [
                          0,
                          -5.892
                        ],
                        [
                          5.892,
                          0
                        ]
                      ],
                      "v": [
                        [
                          10.669,
                          0
                        ],
                        [
                          0,
                          10.669
                        ],
                        [
                          -10.669,
                          0
                        ],
                        [
                          0,
                          -10.669
                        ]
                      ],
                      "c": true
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            }
          ],
          "ip": 0,
          "op": 900.000036657751,
          "st": 0,
          "bm": 0
        },
        {
          "ddd": 0,
          "ind": 4,
          "ty": 4,
          "nm": "Icon Set 3",
          "sr": 1,
          "ks": {
            "o": {
              "a": 0,
              "k": 100,
              "ix": 11
            },
            "r": {
              "a": 0,
              "k": 0,
              "ix": 10
            },
            "p": {
              "a": 0,
              "k": [
                10.393,
                60.974,
                0
              ],
              "ix": 2
            },
            "a": {
              "a": 0,
              "k": [
                0,
                0,
                0
              ],
              "ix": 1
            },
            "s": {
              "a": 0,
              "k": [
                100,
                100,
                100
              ],
              "ix": 6
            }
          },
          "ao": 0,
          "shapes": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "v": [
                        [
                          0,
                          -7.275
                        ],
                        [
                          0,
                          7.275
                        ]
                      ],
                      "c": false
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            }
          ],
          "ip": 0,
          "op": 900.000036657751,
          "st": 0,
          "bm": 0
        },
        {
          "ddd": 0,
          "ind": 5,
          "ty": 4,
          "nm": "Icon Set 2",
          "sr": 1,
          "ks": {
            "o": {
              "a": 0,
              "k": 100,
              "ix": 11
            },
            "r": {
              "a": 0,
              "k": 0,
              "ix": 10
            },
            "p": {
              "a": 0,
              "k": [
                33.375,
                60.974,
                0
              ],
              "ix": 2
            },
            "a": {
              "a": 0,
              "k": [
                0,
                0,
                0
              ],
              "ix": 1
            },
            "s": {
              "a": 0,
              "k": [
                100,
                100,
                100
              ],
              "ix": 6
            }
          },
          "ao": 0,
          "shapes": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "v": [
                        [
                          0,
                          -7.275
                        ],
                        [
                          0,
                          7.275
                        ]
                      ],
                      "c": false
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            }
          ],
          "ip": 0,
          "op": 900.000036657751,
          "st": 0,
          "bm": 0
        }
      ]
    }
  ],
  "layers": [
    {
      "ddd": 0,
      "ind": 1,
      "ty": 0,
      "nm": "people",
      "refId": "comp_0",
      "sr": 1,
      "ks": {
        "o": {
          "a": 1,
          "k": [
            {
              "i": {
                "x": [
                  0.833
                ],
                "y": [
                  0.833
                ]
              },
              "o": {
                "x": [
                  0.167
                ],
                "y": [
                  0.167
                ]
              },
              "t": 20,
              "s": [
                0
              ]
            },
            {
              "t": 25.0000010182709,
              "s": [
                100
              ]
            }
          ],
          "ix": 11
        },
        "r": {
          "a": 0,
          "k": 0,
          "ix": 10
        },
        "p": {
          "a": 1,
          "k": [
            {
              "i": {
                "x": 0.833,
                "y": 0.833
              },
              "o": {
                "x": 0.167,
                "y": 0.167
              },
              "t": 10,
              "s": [
                72,
                210.5,
                0
              ],
              "to": [
                0,
                -21.25,
                0
              ],
              "ti": [
                0,
                21.25,
                0
              ]
            },
            {
              "t": 25.0000010182709,
              "s": [
                72,
                83,
                0
              ]
            }
          ],
          "ix": 2,
          "x": "var $bm_rt;\nvar amp, freq, decay, n, n, t, t, v;\ntry {\n    amp = $bm_div(effect('Elastic Controller Position')(1), 200);\n    freq = $bm_div(effect('Elastic Controller Position')(2), 30);\n    decay = $bm_div(effect('Elastic Controller Position')(3), 10);\n    $bm_rt = n = 0;\n    if (numKeys > 0) {\n        $bm_rt = n = nearestKey(time).index;\n        if (key(n).time > time) {\n            n--;\n        }\n    }\n    if (n == 0) {\n        $bm_rt = t = 0;\n    } else {\n        $bm_rt = t = $bm_sub(time, key(n).time);\n    }\n    if (n > 0) {\n        v = velocityAtTime($bm_sub(key(n).time, $bm_div(thisComp.frameDuration, 10)));\n        $bm_rt = $bm_sum(value, $bm_div($bm_mul($bm_mul(v, amp), Math.sin($bm_mul($bm_mul($bm_mul(freq, t), 2), Math.PI))), Math.exp($bm_mul(decay, t))));\n    } else {\n        $bm_rt = value;\n    }\n} catch (e$$4) {\n    $bm_rt = value = value;\n}"
        },
        "a": {
          "a": 0,
          "k": [
            22,
            70,
            0
          ],
          "ix": 1
        },
        "s": {
          "a": 0,
          "k": [
            100,
            100,
            100
          ],
          "ix": 6
        }
      },
      "ao": 0,
      "ef": [
        {
          "ty": 5,
          "nm": "Elastic Controller Position",
          "np": 5,
          "mn": "Pseudo/MDS Elastic Controller",
          "ix": 1,
          "en": 1,
          "ef": [
            {
              "ty": 0,
              "nm": "Amplitude",
              "mn": "Pseudo/MDS Elastic Controller-0001",
              "ix": 1,
              "v": {
                "a": 0,
                "k": 20,
                "ix": 1
              }
            },
            {
              "ty": 0,
              "nm": "Frequency",
              "mn": "Pseudo/MDS Elastic Controller-0002",
              "ix": 2,
              "v": {
                "a": 0,
                "k": 40,
                "ix": 2
              }
            },
            {
              "ty": 0,
              "nm": "Decay",
              "mn": "Pseudo/MDS Elastic Controller-0003",
              "ix": 3,
              "v": {
                "a": 0,
                "k": 60,
                "ix": 3
              }
            }
          ]
        }
      ],
      "w": 44,
      "h": 70,
      "ip": 0,
      "op": 900.000036657751,
      "st": 0,
      "bm": 0
    },
    {
      "ddd": 0,
      "ind": 2,
      "ty": 4,
      "nm": "right",
      "sr": 1,
      "ks": {
        "o": {
          "a": 0,
          "k": 100,
          "ix": 11
        },
        "r": {
          "a": 1,
          "k": [
            {
              "i": {
                "x": [
                  0.833
                ],
                "y": [
                  0.833
                ]
              },
              "o": {
                "x": [
                  0.167
                ],
                "y": [
                  0.167
                ]
              },
              "t": 0,
              "s": [
                116
              ]
            },
            {
              "t": 10.0000004073083,
              "s": [
                0
              ]
            }
          ],
          "ix": 10,
          "x": "var $bm_rt;\nvar amp, freq, decay, n, n, t, t, v;\ntry {\n    amp = $bm_div(effect('Elastic Controller Rotation')(1), 200);\n    freq = $bm_div(effect('Elastic Controller Rotation')(2), 30);\n    decay = $bm_div(effect('Elastic Controller Rotation')(3), 10);\n    $bm_rt = n = 0;\n    if (numKeys > 0) {\n        $bm_rt = n = nearestKey(time).index;\n        if (key(n).time > time) {\n            n--;\n        }\n    }\n    if (n == 0) {\n        $bm_rt = t = 0;\n    } else {\n        $bm_rt = t = $bm_sub(time, key(n).time);\n    }\n    if (n > 0) {\n        v = velocityAtTime($bm_sub(key(n).time, $bm_div(thisComp.frameDuration, 10)));\n        $bm_rt = $bm_sum(value, $bm_div($bm_mul($bm_mul(v, amp), Math.sin($bm_mul($bm_mul($bm_mul(freq, t), 2), Math.PI))), Math.exp($bm_mul(decay, t))));\n    } else {\n        $bm_rt = value;\n    }\n} catch (e$$4) {\n    $bm_rt = value = value;\n}"
        },
        "p": {
          "a": 0,
          "k": [
            77.794,
            134.659,
            0
          ],
          "ix": 2
        },
        "a": {
          "a": 0,
          "k": [
            116.794,
            469.659,
            0
          ],
          "ix": 1
        },
        "s": {
          "a": 0,
          "k": [
            100,
            100,
            100
          ],
          "ix": 6
        }
      },
      "ao": 0,
      "ef": [
        {
          "ty": 5,
          "nm": "Elastic Controller Rotation",
          "np": 5,
          "mn": "Pseudo/MDS Elastic Controller",
          "ix": 1,
          "en": 1,
          "ef": [
            {
              "ty": 0,
              "nm": "Amplitude",
              "mn": "Pseudo/MDS Elastic Controller-0001",
              "ix": 1,
              "v": {
                "a": 0,
                "k": 20,
                "ix": 1
              }
            },
            {
              "ty": 0,
              "nm": "Frequency",
              "mn": "Pseudo/MDS Elastic Controller-0002",
              "ix": 2,
              "v": {
                "a": 0,
                "k": 40,
                "ix": 2
              }
            },
            {
              "ty": 0,
              "nm": "Decay",
              "mn": "Pseudo/MDS Elastic Controller-0003",
              "ix": 3,
              "v": {
                "a": 0,
                "k": 60,
                "ix": 3
              }
            }
          ]
        },
        {
          "ty": 5,
          "nm": "Elastic Controller Rotation",
          "np": 5,
          "mn": "Pseudo/MDS Elastic Controller",
          "ix": 2,
          "en": 1,
          "ef": [
            {
              "ty": 0,
              "nm": "Amplitude",
              "mn": "Pseudo/MDS Elastic Controller-0001",
              "ix": 1,
              "v": {
                "a": 0,
                "k": 20,
                "ix": 1
              }
            },
            {
              "ty": 0,
              "nm": "Frequency",
              "mn": "Pseudo/MDS Elastic Controller-0002",
              "ix": 2,
              "v": {
                "a": 0,
                "k": 40,
                "ix": 2
              }
            },
            {
              "ty": 0,
              "nm": "Decay",
              "mn": "Pseudo/MDS Elastic Controller-0003",
              "ix": 3,
              "v": {
                "a": 0,
                "k": 60,
                "ix": 3
              }
            }
          ]
        }
      ],
      "shapes": [
        {
          "ty": "gr",
          "it": [
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "v": [
                        [
                          13.366,
                          4.725
                        ],
                        [
                          -13.366,
                          -4.725
                        ]
                      ],
                      "c": false
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      130.161,
                      457.082
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            },
            {
              "ty": "gr",
              "it": [
                {
                  "ind": 0,
                  "ty": "sh",
                  "ix": 1,
                  "ks": {
                    "a": 0,
                    "k": {
                      "i": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "o": [
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ],
                        [
                          0,
                          0
                        ]
                      ],
                      "v": [
                        [
                          -11.565,
                          5.821
                        ],
                        [
                          9.022,
                          12.157
                        ],
                        [
                          14.564,
                          -1.673
                        ],
                        [
                          -14.564,
                          -12.157
                        ],
                        [
                          -14.564,
                          -5.145
                        ]
                      ],
                      "c": false
                    },
                    "ix": 2
                  },
                  "nm": "Path 1",
                  "mn": "ADBE Vector Shape - Group",
                  "hd": false
                },
                {
                  "ty": "st",
                  "c": {
                    "a": 0,
                    "k": [
                      0,
                      0.290196090937,
                      0.560784339905,
                      1
                    ],
                    "ix": 3
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 4
                  },
                  "w": {
                    "a": 0,
                    "k": 3,
                    "ix": 5
                  },
                  "lc": 2,
                  "lj": 2,
                  "bm": 0,
                  "nm": "Stroke 1",
                  "mn": "ADBE Vector Graphic - Stroke",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      131.359,
                      457.502
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      0,
                      0
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 2",
              "np": 2,
              "cix": 2,
              "bm": 0,
              "ix": 2,
              "mn": "ADBE Vector Group",
              "hd": false
            },
            {
              "ty": "gr",
              "it": [
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -0.702,
                              -3.752
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              -3.079,
                              2.256
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              2.563,
                              -15.947
                            ],
                            [
                              2.23,
                              -15.703
                            ],
                            [
                              -1.658,
                              -5.873
                            ],
                            [
                              0.459,
                              5.432
                            ],
                            [
                              -2.563,
                              15.947
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          155.888,
                          392.093
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 1",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 1,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -2.207,
                              -8.71
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              3.575,
                              -12.966
                            ],
                            [
                              1.211,
                              -20.263
                            ],
                            [
                              -3.12,
                              -9.151
                            ],
                            [
                              0.541,
                              9.938
                            ],
                            [
                              -2.382,
                              20.263
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          162.014,
                          389.135
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 2",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 2,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -1.303,
                              -7.711
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              3.385,
                              20.366
                            ],
                            [
                              6.384,
                              6.661
                            ],
                            [
                              -2.421,
                              -20.366
                            ],
                            [
                              -6.169,
                              -11.389
                            ],
                            [
                              -2.174,
                              6.191
                            ],
                            [
                              -4.899,
                              17.305
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          171.192,
                          393.688
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 3",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 3,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              -0.415,
                              -0.036
                            ],
                            [
                              -4.279,
                              5.48
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              4.947,
                              -8.596
                            ],
                            [
                              6.865,
                              -1.712
                            ],
                            [
                              0.445,
                              -0.092
                            ],
                            [
                              2.088,
                              -3.957
                            ]
                          ],
                          "o": [
                            [
                              0.43,
                              0.059
                            ],
                            [
                              6.927,
                              0.608
                            ],
                            [
                              8.804,
                              -11.276
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -3.531,
                              6.133
                            ],
                            [
                              -0.424,
                              0.105
                            ],
                            [
                              -4.504,
                              0.926
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              -14.739,
                              -5.76
                            ],
                            [
                              -13.471,
                              -5.618
                            ],
                            [
                              4.48,
                              -13.647
                            ],
                            [
                              19.989,
                              -17.883
                            ],
                            [
                              7.775,
                              -0.572
                            ],
                            [
                              -8.347,
                              11.939
                            ],
                            [
                              -9.65,
                              12.235
                            ],
                            [
                              -19.989,
                              20.054
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          161.161,
                          434.019
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 4",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 4,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -7.291,
                              3.382
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -1.07,
                              3.772
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -2.963,
                              1.836
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              2.188,
                              -7.734
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              3.557,
                              -1.649
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              -0.458,
                              -3.454
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              -18.805,
                              29.101
                            ],
                            [
                              -16.379,
                              20.526
                            ],
                            [
                              -1.446,
                              3.016
                            ],
                            [
                              4.573,
                              0.224
                            ],
                            [
                              11.86,
                              -8.314
                            ],
                            [
                              14.772,
                              -18.589
                            ],
                            [
                              14.543,
                              -20.318
                            ],
                            [
                              18.658,
                              -29.009
                            ],
                            [
                              18.805,
                              -29.101
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          135.599,
                          416.248
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 5",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 5,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      135.599,
                      416.248
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      135.599,
                      416.248
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 3",
              "np": 5,
              "cix": 2,
              "bm": 0,
              "ix": 3,
              "mn": "ADBE Vector Group",
              "hd": false
            },
            {
              "ty": "tr",
              "p": {
                "a": 0,
                "k": [
                  135.599,
                  416.248
                ],
                "ix": 2
              },
              "a": {
                "a": 0,
                "k": [
                  135.599,
                  416.248
                ],
                "ix": 1
              },
              "s": {
                "a": 0,
                "k": [
                  100,
                  100
                ],
                "ix": 3
              },
              "r": {
                "a": 0,
                "k": 0,
                "ix": 6
              },
              "o": {
                "a": 0,
                "k": 100,
                "ix": 7
              },
              "sk": {
                "a": 0,
                "k": 0,
                "ix": 4
              },
              "sa": {
                "a": 0,
                "k": 0,
                "ix": 5
              },
              "nm": "Transform"
            }
          ],
          "nm": "Group 1",
          "np": 3,
          "cix": 2,
          "bm": 0,
          "ix": 1,
          "mn": "ADBE Vector Group",
          "hd": false
        }
      ],
      "ip": 0,
      "op": 900.000036657751,
      "st": 0,
      "bm": 0
    },
    {
      "ddd": 0,
      "ind": 3,
      "ty": 4,
      "nm": "left",
      "sr": 1,
      "ks": {
        "o": {
          "a": 0,
          "k": 100,
          "ix": 11
        },
        "r": {
          "a": 1,
          "k": [
            {
              "i": {
                "x": [
                  0.833
                ],
                "y": [
                  0.833
                ]
              },
              "o": {
                "x": [
                  0.167
                ],
                "y": [
                  0.167
                ]
              },
              "t": 0,
              "s": [
                -120
              ]
            },
            {
              "t": 10.0000004073083,
              "s": [
                0
              ]
            }
          ],
          "ix": 10,
          "x": "var $bm_rt;\nvar amp, freq, decay, n, n, t, t, v;\ntry {\n    amp = $bm_div(effect('Elastic Controller Rotation')(1), 200);\n    freq = $bm_div(effect('Elastic Controller Rotation')(2), 30);\n    decay = $bm_div(effect('Elastic Controller Rotation')(3), 10);\n    $bm_rt = n = 0;\n    if (numKeys > 0) {\n        $bm_rt = n = nearestKey(time).index;\n        if (key(n).time > time) {\n            n--;\n        }\n    }\n    if (n == 0) {\n        $bm_rt = t = 0;\n    } else {\n        $bm_rt = t = $bm_sub(time, key(n).time);\n    }\n    if (n > 0) {\n        v = velocityAtTime($bm_sub(key(n).time, $bm_div(thisComp.frameDuration, 10)));\n        $bm_rt = $bm_sum(value, $bm_div($bm_mul($bm_mul(v, amp), Math.sin($bm_mul($bm_mul($bm_mul(freq, t), 2), Math.PI))), Math.exp($bm_mul(decay, t))));\n    } else {\n        $bm_rt = value;\n    }\n} catch (e$$4) {\n    $bm_rt = value = value;\n}"
        },
        "p": {
          "a": 0,
          "k": [
            66.285,
            134.659,
            0
          ],
          "ix": 2
        },
        "a": {
          "a": 0,
          "k": [
            105.285,
            469.659,
            0
          ],
          "ix": 1
        },
        "s": {
          "a": 0,
          "k": [
            100,
            100,
            100
          ],
          "ix": 6
        }
      },
      "ao": 0,
      "ef": [
        {
          "ty": 5,
          "nm": "Elastic Controller Rotation",
          "np": 5,
          "mn": "Pseudo/MDS Elastic Controller",
          "ix": 1,
          "en": 1,
          "ef": [
            {
              "ty": 0,
              "nm": "Amplitude",
              "mn": "Pseudo/MDS Elastic Controller-0001",
              "ix": 1,
              "v": {
                "a": 0,
                "k": 20,
                "ix": 1
              }
            },
            {
              "ty": 0,
              "nm": "Frequency",
              "mn": "Pseudo/MDS Elastic Controller-0002",
              "ix": 2,
              "v": {
                "a": 0,
                "k": 40,
                "ix": 2
              }
            },
            {
              "ty": 0,
              "nm": "Decay",
              "mn": "Pseudo/MDS Elastic Controller-0003",
              "ix": 3,
              "v": {
                "a": 0,
                "k": 60,
                "ix": 3
              }
            }
          ]
        },
        {
          "ty": 5,
          "nm": "Elastic Controller Rotation",
          "np": 5,
          "mn": "Pseudo/MDS Elastic Controller",
          "ix": 2,
          "en": 1,
          "ef": [
            {
              "ty": 0,
              "nm": "Amplitude",
              "mn": "Pseudo/MDS Elastic Controller-0001",
              "ix": 1,
              "v": {
                "a": 0,
                "k": 20,
                "ix": 1
              }
            },
            {
              "ty": 0,
              "nm": "Frequency",
              "mn": "Pseudo/MDS Elastic Controller-0002",
              "ix": 2,
              "v": {
                "a": 0,
                "k": 40,
                "ix": 2
              }
            },
            {
              "ty": 0,
              "nm": "Decay",
              "mn": "Pseudo/MDS Elastic Controller-0003",
              "ix": 3,
              "v": {
                "a": 0,
                "k": 60,
                "ix": 3
              }
            }
          ]
        }
      ],
      "shapes": [
        {
          "ty": "gr",
          "it": [
            {
              "ty": "gr",
              "it": [
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              -13.366,
                              4.725
                            ],
                            [
                              13.366,
                              -4.725
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          91.917,
                          457.082
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 1",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 1,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ind": 0,
                      "ty": "sh",
                      "ix": 1,
                      "ks": {
                        "a": 0,
                        "k": {
                          "i": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "o": [
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ],
                            [
                              0,
                              0
                            ]
                          ],
                          "v": [
                            [
                              11.564,
                              5.821
                            ],
                            [
                              -9.023,
                              12.157
                            ],
                            [
                              -14.563,
                              -1.673
                            ],
                            [
                              14.563,
                              -12.157
                            ],
                            [
                              14.563,
                              -5.145
                            ]
                          ],
                          "c": false
                        },
                        "ix": 2
                      },
                      "nm": "Path 1",
                      "mn": "ADBE Vector Shape - Group",
                      "hd": false
                    },
                    {
                      "ty": "st",
                      "c": {
                        "a": 0,
                        "k": [
                          0,
                          0.290196090937,
                          0.560784339905,
                          1
                        ],
                        "ix": 3
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 4
                      },
                      "w": {
                        "a": 0,
                        "k": 3,
                        "ix": 5
                      },
                      "lc": 2,
                      "lj": 2,
                      "bm": 0,
                      "nm": "Stroke 1",
                      "mn": "ADBE Vector Graphic - Stroke",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          90.72,
                          457.502
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          0,
                          0
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 2",
                  "np": 2,
                  "cix": 2,
                  "bm": 0,
                  "ix": 2,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "gr",
                  "it": [
                    {
                      "ty": "gr",
                      "it": [
                        {
                          "ind": 0,
                          "ty": "sh",
                          "ix": 1,
                          "ks": {
                            "a": 0,
                            "k": {
                              "i": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0.704,
                                  -3.752
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "o": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  3.079,
                                  2.256
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "v": [
                                [
                                  -2.563,
                                  -15.947
                                ],
                                [
                                  -2.231,
                                  -15.703
                                ],
                                [
                                  1.658,
                                  -5.873
                                ],
                                [
                                  -0.46,
                                  5.432
                                ],
                                [
                                  2.563,
                                  15.947
                                ]
                              ],
                              "c": false
                            },
                            "ix": 2
                          },
                          "nm": "Path 1",
                          "mn": "ADBE Vector Shape - Group",
                          "hd": false
                        },
                        {
                          "ty": "st",
                          "c": {
                            "a": 0,
                            "k": [
                              0,
                              0.290196090937,
                              0.560784339905,
                              1
                            ],
                            "ix": 3
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 4
                          },
                          "w": {
                            "a": 0,
                            "k": 3,
                            "ix": 5
                          },
                          "lc": 2,
                          "lj": 2,
                          "bm": 0,
                          "nm": "Stroke 1",
                          "mn": "ADBE Vector Graphic - Stroke",
                          "hd": false
                        },
                        {
                          "ty": "tr",
                          "p": {
                            "a": 0,
                            "k": [
                              66.19,
                              392.093
                            ],
                            "ix": 2
                          },
                          "a": {
                            "a": 0,
                            "k": [
                              0,
                              0
                            ],
                            "ix": 1
                          },
                          "s": {
                            "a": 0,
                            "k": [
                              100,
                              100
                            ],
                            "ix": 3
                          },
                          "r": {
                            "a": 0,
                            "k": 0,
                            "ix": 6
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 7
                          },
                          "sk": {
                            "a": 0,
                            "k": 0,
                            "ix": 4
                          },
                          "sa": {
                            "a": 0,
                            "k": 0,
                            "ix": 5
                          },
                          "nm": "Transform"
                        }
                      ],
                      "nm": "Group 1",
                      "np": 2,
                      "cix": 2,
                      "bm": 0,
                      "ix": 1,
                      "mn": "ADBE Vector Group",
                      "hd": false
                    },
                    {
                      "ty": "gr",
                      "it": [
                        {
                          "ind": 0,
                          "ty": "sh",
                          "ix": 1,
                          "ks": {
                            "a": 0,
                            "k": {
                              "i": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  2.206,
                                  -8.71
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "o": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "v": [
                                [
                                  -3.575,
                                  -12.966
                                ],
                                [
                                  -1.212,
                                  -20.263
                                ],
                                [
                                  3.12,
                                  -9.151
                                ],
                                [
                                  -0.541,
                                  9.938
                                ],
                                [
                                  2.381,
                                  20.263
                                ]
                              ],
                              "c": false
                            },
                            "ix": 2
                          },
                          "nm": "Path 1",
                          "mn": "ADBE Vector Shape - Group",
                          "hd": false
                        },
                        {
                          "ty": "st",
                          "c": {
                            "a": 0,
                            "k": [
                              0,
                              0.290196090937,
                              0.560784339905,
                              1
                            ],
                            "ix": 3
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 4
                          },
                          "w": {
                            "a": 0,
                            "k": 3,
                            "ix": 5
                          },
                          "lc": 2,
                          "lj": 2,
                          "bm": 0,
                          "nm": "Stroke 1",
                          "mn": "ADBE Vector Graphic - Stroke",
                          "hd": false
                        },
                        {
                          "ty": "tr",
                          "p": {
                            "a": 0,
                            "k": [
                              60.064,
                              389.135
                            ],
                            "ix": 2
                          },
                          "a": {
                            "a": 0,
                            "k": [
                              0,
                              0
                            ],
                            "ix": 1
                          },
                          "s": {
                            "a": 0,
                            "k": [
                              100,
                              100
                            ],
                            "ix": 3
                          },
                          "r": {
                            "a": 0,
                            "k": 0,
                            "ix": 6
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 7
                          },
                          "sk": {
                            "a": 0,
                            "k": 0,
                            "ix": 4
                          },
                          "sa": {
                            "a": 0,
                            "k": 0,
                            "ix": 5
                          },
                          "nm": "Transform"
                        }
                      ],
                      "nm": "Group 2",
                      "np": 2,
                      "cix": 2,
                      "bm": 0,
                      "ix": 2,
                      "mn": "ADBE Vector Group",
                      "hd": false
                    },
                    {
                      "ty": "gr",
                      "it": [
                        {
                          "ind": 0,
                          "ty": "sh",
                          "ix": 1,
                          "ks": {
                            "a": 0,
                            "k": {
                              "i": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  1.305,
                                  -7.711
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "o": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "v": [
                                [
                                  -3.387,
                                  20.366
                                ],
                                [
                                  -6.383,
                                  6.661
                                ],
                                [
                                  2.421,
                                  -20.366
                                ],
                                [
                                  6.168,
                                  -11.389
                                ],
                                [
                                  2.174,
                                  6.191
                                ],
                                [
                                  4.9,
                                  17.305
                                ]
                              ],
                              "c": false
                            },
                            "ix": 2
                          },
                          "nm": "Path 1",
                          "mn": "ADBE Vector Shape - Group",
                          "hd": false
                        },
                        {
                          "ty": "st",
                          "c": {
                            "a": 0,
                            "k": [
                              0,
                              0.290196090937,
                              0.560784339905,
                              1
                            ],
                            "ix": 3
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 4
                          },
                          "w": {
                            "a": 0,
                            "k": 3,
                            "ix": 5
                          },
                          "lc": 2,
                          "lj": 2,
                          "bm": 0,
                          "nm": "Stroke 1",
                          "mn": "ADBE Vector Graphic - Stroke",
                          "hd": false
                        },
                        {
                          "ty": "tr",
                          "p": {
                            "a": 0,
                            "k": [
                              50.887,
                              393.688
                            ],
                            "ix": 2
                          },
                          "a": {
                            "a": 0,
                            "k": [
                              0,
                              0
                            ],
                            "ix": 1
                          },
                          "s": {
                            "a": 0,
                            "k": [
                              100,
                              100
                            ],
                            "ix": 3
                          },
                          "r": {
                            "a": 0,
                            "k": 0,
                            "ix": 6
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 7
                          },
                          "sk": {
                            "a": 0,
                            "k": 0,
                            "ix": 4
                          },
                          "sa": {
                            "a": 0,
                            "k": 0,
                            "ix": 5
                          },
                          "nm": "Transform"
                        }
                      ],
                      "nm": "Group 3",
                      "np": 2,
                      "cix": 2,
                      "bm": 0,
                      "ix": 3,
                      "mn": "ADBE Vector Group",
                      "hd": false
                    },
                    {
                      "ty": "gr",
                      "it": [
                        {
                          "ind": 0,
                          "ty": "sh",
                          "ix": 1,
                          "ks": {
                            "a": 0,
                            "k": {
                              "i": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0.416,
                                  -0.036
                                ],
                                [
                                  4.279,
                                  5.48
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  -4.949,
                                  -8.596
                                ],
                                [
                                  -6.865,
                                  -1.712
                                ],
                                [
                                  -0.445,
                                  -0.092
                                ],
                                [
                                  -2.089,
                                  -3.957
                                ]
                              ],
                              "o": [
                                [
                                  -0.43,
                                  0.059
                                ],
                                [
                                  -6.925,
                                  0.608
                                ],
                                [
                                  -8.806,
                                  -11.276
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  3.529,
                                  6.133
                                ],
                                [
                                  0.424,
                                  0.105
                                ],
                                [
                                  4.504,
                                  0.926
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "v": [
                                [
                                  14.738,
                                  -5.76
                                ],
                                [
                                  13.47,
                                  -5.618
                                ],
                                [
                                  -4.479,
                                  -13.647
                                ],
                                [
                                  -19.989,
                                  -17.883
                                ],
                                [
                                  -7.775,
                                  -0.572
                                ],
                                [
                                  8.346,
                                  11.939
                                ],
                                [
                                  9.649,
                                  12.235
                                ],
                                [
                                  19.989,
                                  20.054
                                ]
                              ],
                              "c": false
                            },
                            "ix": 2
                          },
                          "nm": "Path 1",
                          "mn": "ADBE Vector Shape - Group",
                          "hd": false
                        },
                        {
                          "ty": "st",
                          "c": {
                            "a": 0,
                            "k": [
                              0,
                              0.290196090937,
                              0.560784339905,
                              1
                            ],
                            "ix": 3
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 4
                          },
                          "w": {
                            "a": 0,
                            "k": 3,
                            "ix": 5
                          },
                          "lc": 2,
                          "lj": 2,
                          "bm": 0,
                          "nm": "Stroke 1",
                          "mn": "ADBE Vector Graphic - Stroke",
                          "hd": false
                        },
                        {
                          "ty": "tr",
                          "p": {
                            "a": 0,
                            "k": [
                              60.917,
                              434.019
                            ],
                            "ix": 2
                          },
                          "a": {
                            "a": 0,
                            "k": [
                              0,
                              0
                            ],
                            "ix": 1
                          },
                          "s": {
                            "a": 0,
                            "k": [
                              100,
                              100
                            ],
                            "ix": 3
                          },
                          "r": {
                            "a": 0,
                            "k": 0,
                            "ix": 6
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 7
                          },
                          "sk": {
                            "a": 0,
                            "k": 0,
                            "ix": 4
                          },
                          "sa": {
                            "a": 0,
                            "k": 0,
                            "ix": 5
                          },
                          "nm": "Transform"
                        }
                      ],
                      "nm": "Group 4",
                      "np": 2,
                      "cix": 2,
                      "bm": 0,
                      "ix": 4,
                      "mn": "ADBE Vector Group",
                      "hd": false
                    },
                    {
                      "ty": "gr",
                      "it": [
                        {
                          "ind": 0,
                          "ty": "sh",
                          "ix": 1,
                          "ks": {
                            "a": 0,
                            "k": {
                              "i": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  7.291,
                                  3.382
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  1.07,
                                  3.772
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  2.963,
                                  1.836
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "o": [
                                [
                                  0,
                                  0
                                ],
                                [
                                  -2.188,
                                  -7.734
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  -3.555,
                                  -1.649
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0.459,
                                  -3.454
                                ],
                                [
                                  0,
                                  0
                                ],
                                [
                                  0,
                                  0
                                ]
                              ],
                              "v": [
                                [
                                  18.805,
                                  29.101
                                ],
                                [
                                  16.379,
                                  20.526
                                ],
                                [
                                  1.446,
                                  3.016
                                ],
                                [
                                  -4.573,
                                  0.224
                                ],
                                [
                                  -11.86,
                                  -8.314
                                ],
                                [
                                  -14.773,
                                  -18.589
                                ],
                                [
                                  -14.543,
                                  -20.318
                                ],
                                [
                                  -18.657,
                                  -29.009
                                ],
                                [
                                  -18.805,
                                  -29.101
                                ]
                              ],
                              "c": false
                            },
                            "ix": 2
                          },
                          "nm": "Path 1",
                          "mn": "ADBE Vector Shape - Group",
                          "hd": false
                        },
                        {
                          "ty": "st",
                          "c": {
                            "a": 0,
                            "k": [
                              0,
                              0.290196090937,
                              0.560784339905,
                              1
                            ],
                            "ix": 3
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 4
                          },
                          "w": {
                            "a": 0,
                            "k": 3,
                            "ix": 5
                          },
                          "lc": 2,
                          "lj": 2,
                          "bm": 0,
                          "nm": "Stroke 1",
                          "mn": "ADBE Vector Graphic - Stroke",
                          "hd": false
                        },
                        {
                          "ty": "tr",
                          "p": {
                            "a": 0,
                            "k": [
                              86.479,
                              416.248
                            ],
                            "ix": 2
                          },
                          "a": {
                            "a": 0,
                            "k": [
                              0,
                              0
                            ],
                            "ix": 1
                          },
                          "s": {
                            "a": 0,
                            "k": [
                              100,
                              100
                            ],
                            "ix": 3
                          },
                          "r": {
                            "a": 0,
                            "k": 0,
                            "ix": 6
                          },
                          "o": {
                            "a": 0,
                            "k": 100,
                            "ix": 7
                          },
                          "sk": {
                            "a": 0,
                            "k": 0,
                            "ix": 4
                          },
                          "sa": {
                            "a": 0,
                            "k": 0,
                            "ix": 5
                          },
                          "nm": "Transform"
                        }
                      ],
                      "nm": "Group 5",
                      "np": 2,
                      "cix": 2,
                      "bm": 0,
                      "ix": 5,
                      "mn": "ADBE Vector Group",
                      "hd": false
                    },
                    {
                      "ty": "tr",
                      "p": {
                        "a": 0,
                        "k": [
                          86.479,
                          416.248
                        ],
                        "ix": 2
                      },
                      "a": {
                        "a": 0,
                        "k": [
                          86.479,
                          416.248
                        ],
                        "ix": 1
                      },
                      "s": {
                        "a": 0,
                        "k": [
                          100,
                          100
                        ],
                        "ix": 3
                      },
                      "r": {
                        "a": 0,
                        "k": 0,
                        "ix": 6
                      },
                      "o": {
                        "a": 0,
                        "k": 100,
                        "ix": 7
                      },
                      "sk": {
                        "a": 0,
                        "k": 0,
                        "ix": 4
                      },
                      "sa": {
                        "a": 0,
                        "k": 0,
                        "ix": 5
                      },
                      "nm": "Transform"
                    }
                  ],
                  "nm": "Group 3",
                  "np": 5,
                  "cix": 2,
                  "bm": 0,
                  "ix": 3,
                  "mn": "ADBE Vector Group",
                  "hd": false
                },
                {
                  "ty": "tr",
                  "p": {
                    "a": 0,
                    "k": [
                      86.479,
                      416.248
                    ],
                    "ix": 2
                  },
                  "a": {
                    "a": 0,
                    "k": [
                      86.479,
                      416.248
                    ],
                    "ix": 1
                  },
                  "s": {
                    "a": 0,
                    "k": [
                      100,
                      100
                    ],
                    "ix": 3
                  },
                  "r": {
                    "a": 0,
                    "k": 0,
                    "ix": 6
                  },
                  "o": {
                    "a": 0,
                    "k": 100,
                    "ix": 7
                  },
                  "sk": {
                    "a": 0,
                    "k": 0,
                    "ix": 4
                  },
                  "sa": {
                    "a": 0,
                    "k": 0,
                    "ix": 5
                  },
                  "nm": "Transform"
                }
              ],
              "nm": "Group 1",
              "np": 3,
              "cix": 2,
              "bm": 0,
              "ix": 1,
              "mn": "ADBE Vector Group",
              "hd": false
            },
            {
              "ty": "tr",
              "p": {
                "a": 0,
                "k": [
                  86.479,
                  416.248
                ],
                "ix": 2
              },
              "a": {
                "a": 0,
                "k": [
                  86.479,
                  416.248
                ],
                "ix": 1
              },
              "s": {
                "a": 0,
                "k": [
                  100,
                  100
                ],
                "ix": 3
              },
              "r": {
                "a": 0,
                "k": 0,
                "ix": 6
              },
              "o": {
                "a": 0,
                "k": 100,
                "ix": 7
              },
              "sk": {
                "a": 0,
                "k": 0,
                "ix": 4
              },
              "sa": {
                "a": 0,
                "k": 0,
                "ix": 5
              },
              "nm": "Transform"
            }
          ],
          "nm": "Group 2",
          "np": 1,
          "cix": 2,
          "bm": 0,
          "ix": 1,
          "mn": "ADBE Vector Group",
          "hd": false
        }
      ],
      "ip": 0,
      "op": 900.000036657751,
      "st": 0,
      "bm": 0
    }
  ],
  "markers": []
}