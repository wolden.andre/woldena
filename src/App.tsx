import * as React from 'react';
import './App.css';
import RenameMe from "./comingSoon";
import LottieControl from "./comingSoon/lottieExample";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src="./static/image3.png" className="App-logo" alt="logo" />
        Coming soon ...
      </header>
    </div>
  );
}

export default App;
